#include <bits/stdc++.h>
using namespace std;

int main () {

	string R;
	cin >> R;
	string res = "";

	for (int i = 0; i < R.size(); ++i) {
		if ((R[i] == 'a') || (R[i] == 'e') || (R[i] == 'i') || (R[i] == 'o') || (R[i] == 'u')) {
			res += R[i];
		}
	}

	string rev = "";

	for (int i = 0; i < res.size(); ++i) {
		rev = res[i] + rev;
	}

	if (rev == res) {
		cout << 'S';
	}
	else {
		cout << 'N';

	}
}