#include <bits/stdc++.h>
using namespace std;


int main () {
	int N;
	int M;
	cin >> N >> M;
	int V[55][55];
	int wcol[55];
	int wlin[55];
	int bcol[55];
	int blin[55];
	long long totalw = 0;
	long long totalb = 0;
	long long total = 0;
	
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < M; j++) {
			cin >> V[i][j];
			if (V[i][j] == 0) {
				wlin[i] += 1;
				wcol[j] += 1;
				blin[i] += 0;
				bcol[j] += 0;
			}
			else {
				wlin[i] += 0;
				wcol[j] += 0;
				blin[i] += 1;
				bcol[j] += 1;
			}
		}
	}

	for (int i = 0; i < N; i++) {
		for (int e = 0; e < blin[i]; e++) {
			totalb += pow(2, e);
		}
		for (int e = 0; e < wlin[i]; e++) {
			totalw += pow(2, e);
		}
	}

	for (int j = 0; j < M; j++) {
		for (int e = 0; e < bcol[j]; e++) {
			totalb += pow(2, e);
		}
		for (int e = 0; e < wcol[j]; e++) {
			totalw += pow(2, e);
		}
	}

	total = totalw + totalb - N*M;
	cout << total;
}