#include <bits/stdc++.h>
using namespace std;

string P[5];

int main () {

	int k;
	int p;

	cin >> k;

	k = k * 2;

	for (int i = 0; i < 4; i++) {
		cin >> P[i];
	}

	int res = 0;

	for (int c = 0; c < 10; c++) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				if (P[i][j] == '.') {
					continue;
				}
				
				int x = P[i][j] - '0';
				if (c == x) {
					res += 1;
				}
			}
		}

		if (res > k) {
			cout << "NO";
			break;
		}
		res = 0;
	}

	if (res == 0) {
		cout << "YES";
	}
}