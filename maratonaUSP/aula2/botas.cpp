#include <bits/stdc++.h>
using namespace std;

int H[10005];

int main () {
	
	int res = 0;

	int N;
	int M;
	char L;
	cin >> N;
	for (int i = 0; i < N; i++) {
		cin >> M >> L;
		
		if (L == 'E') {
			H[i] = M * (-1);
		}
		else {
			H[i] = M;
		}
	}
	
	res = 0;

	for (int a = 1; a < N; a++) {
		for (int b = 0; b < a; b++) {
			if ((H[a] == 0)||(H[b] == 0)) {
				continue;
			}
			if (H[a] + H[b] == 0) {
				H[a] = 0;
				H[b] = 0;
				res += 1;
			}
		}
	}
	cout << res;
}