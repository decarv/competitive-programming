#include <bits/stdc++.h>
using namespace std;

int main () {
	
	int N;
	int v[10][10];
	int sumcols;
	int sumrows;
	int sumdiag;
	int summagic = 0;

	cin >> N;
	
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cin >> v[i][j];
		}
	}

	for (int j = 0; j < N; j++) {
		summagic += v[0][j];
	}

	for (int i = 0; i < N; i++) {
		sumrows = 0;
		for (int j = 0; j < N; j++) {
			sumrows += v[i][j];
		}
		if (sumrows != summagic) {
			summagic = -1;
			break;
		}
	}


	for (int j = 0; j < N; j++) {
			sumcols = 0;
			for (int i = 0; i < N; i++) {
				sumcols += v[i][j];
			}
			if (sumcols != summagic) {
				summagic = -1;
				break;
			}
		}

	sumdiag = 0;
	for (int j = 0; j < N; j++) {
			sumdiag += v[j][j];
			}

	if (sumdiag != summagic) {
		summagic = -1;
		}

	int sumdiag2 = 0;
	int i = 0;
	int j = N-1;
	while (i < N) {
		sumdiag2 += v[i][j];
		j -= 1;
		i += 1;
	}
	if (sumdiag2 != summagic) {
		summagic = -1;
		}

	cout << summagic;
}