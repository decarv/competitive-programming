#include <bits/stdc++.h>
using namespace std;

int V[10000];
int Z[10000];

int main () {
	int N;
	cin >> N;
	int floor;
	int ceil = 0;
	int j = 0;
	int k;
	int n;
	int nzeros = 0;

	for (int i = 0; i < N; i++) {
			cin >> V[i];
			if (V[i] == 0) {
				Z[j] = i;
				j += 1;
				nzeros += 1;
			}
	}

	k = 0;
	for (int i = Z[0] - 1; i > -1; i--) {
		V[i] = k + 1;
		if (k < 8) {
			k += 1;
		}
	}

	for (int i = 1; i < nzeros+1; i++) {
		floor = Z[i-1];
		ceil = Z[i];		
		n = (ceil - floor) / 2;

		k = 0;
		for (int j = floor + 1; j < floor + n + 1; j++) {
			V[j] = k + 1;
			if (k < 8) {
			k += 1;
		}
		}

		k = 0;
		for (int j = ceil - 1; j > ceil - n - 1; j--) {
			V[j] = k + 1;
			if (k < 8) {
			k += 1;
		}
		}

	}
	k = 0;
	for (int i = Z[nzeros-1]; i < N; i++) {
		V[i+1] = k + 1;
		if (k < 8) {
			k += 1;
		} 
	}


	int t = 0;
	while (true) {
		cout << V[t];
		if (t == N - 1) {
			break;
		}
		cout << " ";
		t += 1;
	}
}