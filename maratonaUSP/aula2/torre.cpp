#include <bits/stdc++.h>
using namespace std;


int main () {
	int B[1005][1005];
	int N;
	cin >> N;
	int linsum[1005] = { 0 };
	int colsum[1005] = { 0 };
	int sum = 0;
	int max = 0;

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cin >> B[i][j];
			linsum[i] += B[i][j];
			colsum[j] += B[i][j];
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			sum = linsum[i] + colsum[j] - 2*B[i][j];
			if (sum > max) {
				max = sum;
			}
		}
	}
	cout << max;
}