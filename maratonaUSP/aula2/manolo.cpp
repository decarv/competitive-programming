#include <bits/stdc++.h>
using namespace std;


int main () {
	
	int N;
	cin >> N;
	int P[N+5][N+5];
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cin >> P[i][j];
		}
	}

	int Q;
	cin >> Q;
	int C[Q][4];
	for (int i = 0; i < Q; i++) {
		for (int j = 0; j < 4; j++) {
			cin >> C[i][j];
		}
	}

	int res = 0;

	for (int c = 0; c < Q; c++) {
		for (int i = C[c][0] - 1; i < C[c][2]; i++) {
			for (int j = C[c][1] - 1; j < C[c][3]; j++) {
				res += P[i][j];
				P[i][j] = 0;
			}
		}
	}
	
	cout << res;
}