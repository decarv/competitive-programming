#include <bits/stdc++.h>
using namespace std;

int V[1123];

int main () {
	int N;
	cin >> N;

	for (int i = 0; i < N; i++) {
		cin >> V[i];
	}

	int a = 0;
	int b = 0;
	int i = 0;
	int j = N-1;
	int turn = 1;

	while (true) {
		if (V[i] > V[j]) {
			if (turn > 0) {
				a += V[i];
			}
			else {
				b += V[i];
			}
			i++;
		}
		else if (V[i] < V[j]) {
			if (turn > 0) {
				a += V[j];
			}
			else {
				b += V[j];
			}
			j--;
		}
	
		else {
			if (turn > 0) {
				a += V[j];
			}
			else {
				b += V[j];
			}
			break;
		}

	turn *= (-1);
	}
	cout << a << " " << b; 
