#include <bits/stdc++.h>
using namespace std;

int main() {

	long int F[112346];
	long int O[11235];
	long int P[112346];
	long int res[112346];

	int N, M;
	cin >> N >> M;
	
	for (int i = 0; i < N-1; i++) {
		cin >> F[i];
	}

	for (int i = 0; i < N; i++) {
		cin >> P[i];
	}

	for (int i = 0; i < M; i++) {
		cin >> O[i];
	}

	for (int j = 0; j < M; j++) {
		int l = 0;
		int r = N-2;
		if (O[j] < F[l])
			res[j] = l;
		else if (O[j] >= F[r])
			res[j] = r+1;
		else
			while (true) {
				int mid = (l + r) / 2;

				if (mid == l || O[j] == F[mid]) {
					res[j] = mid + 1;
					break;
				}
				if (O[j] < F[mid]) {
					r = mid;
				}
				else if (O[j] >  F[mid]) {
					l = mid;
				}
			}
	}

	for (int i = 0; i < M-1; i++) {
		cout << P[res[i]] << " ";
	}
	cout << P[res[M-1]];
}
				
