#include <bits/stdc++.h>
using namespace std;

int V[1123];

int main() {
	int n;
	long long l;
	cin >> n >> l;

	for (int i = 0; i < n; i++) {
		cin >> V[i];
		}

	sort(V, V+n);

	double dist = 2*(V[0] - 0);
	double maxdist = 2*(l - V[n-1]);

	if (dist > maxdist) {
		maxdist = dist;
		}

	for (int i = 1; i < n; i++) {
		dist = V[i] - V[i-1];
		if (dist > maxdist) {
			maxdist = dist;
		}
	}
	double radius = maxdist / 2;
	cout.precision(17);
	cout << radius;
}