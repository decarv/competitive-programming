#include <bits/stdc++.h>
using namespace std;

int V[112345];

int main() {
	int N;
	cin >> N;
	for (int i = 0; i<N;i++) {
		cin >> V[i];
	}
	int K;
	cin >> K;

	int i = 0;
	int end = N-1;
	while (true) {
		if (V[i] + V[end] > K) {
			end--;
		}
		else if (V[i] + V[end] < K) {
			i++;
		}
		else {
			break;
		}
	}
	cout << V[i] << " " << V[end];
}