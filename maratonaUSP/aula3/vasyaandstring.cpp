#include <bits/stdc++.h>
using namespace std;

string S;
int n, k;

int check(char x) {

	int r = 0, count = 0, ret = 0;

	for (int l = 0; l < n; l++) {
		while (r < n && (S[r] != x || count < k)) {
			
			if (S[r] == x)
				count++;
			
			r++;
		}

		if (S[l] == x)
			count--;

		if (ret < r-l)
			ret = r-l;

	}
	return ret;
}


int main() {
	cin >> n >> k;
	cin >> S;
	int ans;
	int ansa = check('a');
	int ansb = check('b');
	if (ansa > ansb) {
		ans = ansa;
	}
	else {
		ans = ansb;
	}

	cout << ans;
}