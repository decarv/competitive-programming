#include <bits/stdc++.h>
using namespace std;

int V[112][112] = { 0 };

int main() {
	int N; // numeros de paises
	int M; // numero de modalidades
	cin >> N >> M;


	int ouro, prata, bronze; // indices para cada medalha
	for (int i = 0; i < M; i++) {
		cin >> ouro >> prata >> bronze;
		
		V[ouro-1][1] += 1;
		V[ouro-1][0] = ouro;
		
		V[prata-1][2] += 1;
		V[prata-1][0] = prata;
		
		V[bronze-1][3] += 1;
		V[bronze-1][0] = bronze;
	}

	// selection sort

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i][1] < V[j][1]) {
				for (int k = 0; k < 4; k++) {
					int s = V[i][k];
					V[i][k] = V[j][k];
					V[j][k] = s;
				}
			}
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i][1] == V[j][1]) {
				if (V[i][2] < V[j][2]) {
					for (int k = 0; k < 4; k++) {
						int s = V[i][k];
						V[i][k] = V[j][k];
						V[j][k] = s;
					}
				}
			}
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i][1] == V[j][1]) {
				if (V[i][2] == V[j][2]) {
					if (V[i][3] < V[j][3]) {
						for (int k = 0; k < 4; k++) {
							int s = V[i][k];
							V[i][k] = V[j][k];
							V[j][k] = s;
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i][1] == V[j][1]) {
				if (V[i][2] == V[j][2]) {
					if (V[i][3] == V[j][3]) {
						if (V[i][0] > V[j][0]) {
							for (int k = 0; k < 4; k++) {
								int s = V[i][k];
								V[i][k] = V[j][k];
								V[j][k] = s;
							}
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < N; i++){
		if (i == N-1) {
			cout << V[i][0];
			break;
		}
		cout << V[i][0] << " ";
	}

}