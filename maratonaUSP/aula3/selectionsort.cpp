#include <bits/stdc++.h>
using namespace std;



int V[100];


int main () {
	int N;
	cin >> N;

	for (int i = 0; i < N; i++) {
		cin >> V[i];
	}

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i] < V[j]) {
				int s = V[i];
				V[i] = V[j];
				V[j] = s;
			}
		}
	}

	for (int i = 0; i < N; i++) {
		cout << V[i] << " ";
	}
}