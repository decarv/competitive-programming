#include <bits/stdc++.h>
using namespace std;

int V[112345];

int main() {
	int N;
	cin >> N;
	for (int i = 0; i < N; i++) {
		cin >> V[i];
	}
	sort(V, V+N);
	int i = 0;
	while (true) {
		cout << V[i];
		if (i == N-1) {
			break;
		}
		cout << " ";
		i += 1;
	}
}