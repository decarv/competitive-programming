#include <bits/stdc++.h>
using namespace std;

int V[1123456][2];

int main() {
	int n;
	cin >> n;

	for (int i = 0; i < n; i++) {
		cin >> V[i][0];
		V[i][1] = 1;
	}

	int clawRange = 0;
	int alive = 0;
	for (int i = n-1; i > -1; i--) {

		if (clawRange == 0) {
			alive++;
		}
		else {
			clawRange--;
		}

		if (clawRange < V[i][0]) {
			clawRange = V[i][0];
		}
	}
	cout << alive;
}