#include <bits/stdc++.h>
using namespace std;

int V[112][112];

int main() {
	int N; // numeros de paises
	int M; // numero de modalidades
	cin >> N >> M;

	// definiçao de matriz

	for (int i = 0; i < N; i++) {
		V[i][0] = i+1;
	}

	int ouro, prata, bronze; // indices para cada medalha
	for (int i = 0; i < M; i++) {
		cin >> ouro >> prata >> bronze;
		
		V[ouro-1][1] += 1;
		V[prata-1][2] += 1;
		V[bronze-1][3] += 1;
	}

	// selection sort

	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (V[i][1] < V[j][1]) {
				for (int k = 0; k < 4; k++) {
					int s = V[i][k];
					V[i][k] = V[j][k];
					V[j][k] = s;
				}
			}
			else if (V[i][1] == V[j][1]) {
				if (V[i][2] < V[j][2]) {
					for (int k = 0; k < 4; k++) {
						int s = V[i][k];
						V[i][k] = V[j][k];
						V[j][k] = s;
					}
				}
				else if (V[i][2] == V[j][2]) {
					if (V[i][3] < V[j][3]) {
						for (int k = 0; k < 4; k++) {
							int s = V[i][k];
							V[i][k] = V[j][k];
							V[j][k] = s;
						}
					}
					else if (V[i][3] == V[j][3]) {
						if (V[i][0] > V[j][0]) {
							for (int k = 0; k < 4; k++) {
								int s = V[i][k];
								V[i][k] = V[j][k];
								V[j][k] = s;
							}
						}
					}
				}
			}
		}
	}

	for (int i = 0; i < N; i++){
		if (i == N-1) {
			cout << V[i][0];
			break;
		}
		cout << V[i][0] << " ";
	}

}